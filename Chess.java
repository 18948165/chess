import java.util.*;
import java.io.*;
public class Chess
{
   public static void main(String [] args)
   {
      Piece[][] board = new Piece[8][8];
      board = resetBoard();
      writeToFile(board);
      /*Scanner kb = new Scanner(System.in);
      boolean valid = false;
      while(!valid)
      {
         System.out.println("What would you like to do? (C)ontinue playing/Start a game or (R)eset game");
         String choice = kb.nextLine();
         if(choice.length() > 1 || choice.length() < 1)
         {
            System.out.println("Error. Please enter only 1 letter");
         }
         if(choice.equalsIgnoreCase("C"))
         {
            //continue
            valid = true;
         }
         else if(choice.equalsIgnoreCase("R"))
         {
            //reset
            valid = true;
         }
         else
         {
            System.out.println("Error. Please make sure you enter either 'C' or 'R'.");
         }
         if(!valid)
         {
            System.out.println("Please try again.");
         }
      }*/
   }
   public static Piece[][] resetBoard()
   {
      Piece[][] board = new Piece[8][8];
      board[0][0] = new Rook(board, 0, 0, "BLACK", false);
      board[1][0] = new Knight(board, 1, 0, "BLACK");
      board[2][0] = new Bishop(board, 2, 0, "BLACK");
      board[3][0] = new Queen(board, 3, 0, "BLACK");
      board[4][0] = new King(board, 4, 0, "BLACK", false);
      board[5][0] = new Bishop(board, 5, 0, "BLACK");
      board[6][0] = new Knight(board, 6, 0, "BLACK");
      board[7][0] = new Rook(board, 7, 0, "BLACK", false);
      for(int i = 0; i < 8; i++)
      {
         board[1][i] = new Pawn(board, 1, i, "BLACK", false, false);
      }
      for(int i = 2; i < 6; i++)
      {
         for(int j = 0; i < 8; i++)
         {
            board[i][j] = null;
         }
      }
      for(int i = 0; i < 8; i++)
      {
         board[6][i] = new Pawn(board, 6, i, "WHITE", false, false);
      }
      board[0][7] = new Rook(board, 0, 7, "WHITE", false);
      board[1][7] = new Knight(board, 1, 7, "WHITE");
      board[2][7] = new Bishop(board, 2, 7, "WHITE");
      board[3][7] = new Queen(board, 3, 7, "WHITE");
      board[4][7] = new King(board, 4, 7, "WHITE", false);
      board[5][7] = new Bishop(board, 5, 7, "WHITE");
      board[6][7] = new Knight(board, 6, 7, "WHITE");
      board[7][7] = new Rook(board, 7, 7, "WHITE", false);
      return board;
   }
   public static int getPosition(char x)
   {
      switch(x)
      {
         case 'A':
            return 0;
         case 'B':
            return 1;
         case 'C':
            return 2;
         case 'D':
            return 3;
         case 'E':
            return 4;
         case 'F':
            return 5;
         case 'G':
            return 6;
         case 'H':
            return 7;
      }
      return 0;
   }
   public static void movePiece(Piece[][] board, String moveFrom, String moveTo)
   {
      boolean pieceExists = false;
      boolean moveValid = false;
      int xFrom = 0, yFrom = 0;
      Piece p = null;
      while(!moveValid)
      {
         while(!pieceExists)
         {
            xFrom = getPosition(moveFrom.charAt(0));
            yFrom = moveFrom.charAt(1) - '0';
            p = board[xFrom][yFrom];
            if(p != null)
            {
               pieceExists = true;
            }
         }
         int xTo = getPosition(moveTo.charAt(0));
         int yTo = moveTo.charAt(1) - '0';
         if( xFrom == xTo && yFrom == yTo)
         {
            System.out.println("Error. The square you chose to move from and to are the same square.");
         }
         else
         {
            moveValid = p.moveTo(xTo, xFrom);
         }
      }
   }
   public static void writeToFile(Piece[][] board)
   {
      FileWriter saveFile = new FileWriter("storingData.dat");
      PrintWriter save = new PrintWriter(saveFile);
      String pPiece = "";
      String pTeam = "";
      save.print("[Board:{");
      for(int i = 0; i < 8; i++)
      {
         for(int j = 0; j < 8; j++)
         {
            if(board[i][j] != null)
            {
               Piece p = board[i][j];
               pPiece = p.getPiece();
               pTeam = p.getTeam();  
            }
            else
            {
               pPiece = "null";
               pTeam = "null";
            }
            save.print("{\"Piece\":\"" + pPiece + "\",\"Team\":\"" + pTeam + "\"}");
         }
      }
      save.print("}]");
   }
}