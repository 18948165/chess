## Importing the repository:

## Open the location where you would like the repository to be and then type the following code into GitBash:
	"git clone https://{username}@bitbucket.org/18948165/ice-assessment.git ."
## Type 
	"git status"
## and check the reply starts with "On branch master";

## Committing a change:

## Type
	"git init"
## to reconnect to the bitbucket (always do this).
## Type
	"git status"
## and check that all files you wish to update are displayed in red.
## If they are not: Check that the file was saved correctly.
## To set a file to be added to the gitbucket repository, type the following code:
	"git add {file/folder name}"
## or type:
	"git add ."
## To add all files in the folder to the commit.
## Type 
	"git status"
##	and check the files you want are listed in green.
## Type the following command with your own commit message explaining changes:
	"git commit -m "{commit message}""
## Type 
	"git status"
##	and check that it says:
	"Your branch is ahead of 'origin/master' by x commit."
## Then to finalise the push, type:
	"git push origin master"

## Updating your local copy of the repository to suit the gitbucket repository:

## Type
	"git status"
##	to check that there are changes made.
## Type 
	"git pull {file/folder name} origin master" 
## to get a specific file.
## or type:
	"git pull . origin master"
## to update all files in the repository.
	
## Editing an already entered commit message:

## Type 
	"git commit -e --amend"
## Just like in vim on putty, press "i" to enter insert mode.
## Change the first line to your new commit message and then press "esc" to go back to command mode.
## Type ":wq" and the message is changed.