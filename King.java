public class King extends Piece
{
    private boolean threatened;
    private boolean hasMoved;
    public King(Piece[][] board, int x, int y, String team, boolean hasMoved)
    {
        super(board, x, y, team, false);
        this.threatened = false;
        this.hasMoved = hasMoved;
    }
    public boolean isThreatened()
    {
        return threatened;
    }
    public boolean canMoveTo(int x, int y)
    {
        return true;
    }
    public boolean moveTo(int x, int y, Piece[][] board)
    {
        return true;
    }
    public String getPiece()
    {
         return "King";
    }
    public String getTeam()
    {
        return super.getTeam();
    }
    public boolean getHasMoved()
    {
        return hasMoved;
    }
}