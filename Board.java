public class Board
{
    Piece[][] board;
    public Piece pieceAt(int x, int y)
    {
        Piece requestedPiece = board[x][y];
        return requestedPiece;
    }
    public void setBoard(Piece[][] newBoard)
    {
        this.board = newBoard;
    }
    public Piece[][] getBoard()
    {
        return board;
    }
}

