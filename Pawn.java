public class Pawn extends Piece
{
    private boolean hasMoved;
    private boolean movedTwoLastMove;
    public Pawn(Piece[][] board, int x, int y, String team, boolean hasMoved, boolean movedTwoLastMove)
    {
        super(board, x, y, team, false);
        this.hasMoved = hasMoved;
        this.movedTwoLastMove = movedTwoLastMove;
    }
    public boolean canMoveTo(int x, int y)
    {
        return true;
    }
    public boolean moveTo(int x, int y, Piece[][] piece)
    {
        boolean valid = false;
        boolean taking = false;
        if(super.getTeam().equals("BLACK") && y <= super.getY() )
        {
            System.out.println("Error. Pawns must move at least 1 space forward.");
        }
        if(super.getTeam().equals("WHITE") && y >= super.getY() )
        {
            System.out.println("Error. Pawns must move at least 1 space forward.");
        }
        else if(super.getTeam().equals("BLACK") && hasMoved && y == super.getY()+2 )
        {
            System.out.println("Error. Pawns can only move forward 2 spaces on their first move.");
        }
        else if(super.getTeam().equals("BLACK") && hasMoved && y > super.getY()+2 )
        {
            System.out.println("Error. Pawns cant move more than 2 spaces forward.");
        }
        else if(super.getTeam().equals("WHITE") && hasMoved && y == super.getY()-2 )
        {
            System.out.println("Error. Pawns can only move forward 2 spaces on their first move.");
        }
        else if(super.getTeam().equals("WHITE") && hasMoved && y < super.getY()-2 )
        {
            System.out.println("Error. Pawns cant move more than 2 spaces forward.");
        }
        else if(x >= super.getX()+2 || x <= super.getX()-2)
        {
            System.out.println("Error. Pawns cannot move more than 1 space sideways.");
        }
        else if((y == super.getY()+2 || y == super.getY()-2) && (x == super.getX()+1 || x == super.getX()-1))
        {
            System.out.println("Error. Cannot move 2 spaces forward and 1 space sideways.");
        }
        else if(x == super.getX()+1)
        {
            if(piece[x][y] == null)
            {
                if(piece[x+1][y] != null )
                {
                    taking = true;
                    valid = true;
                    piece[x+1][y] = null;
                }
            }
            else
            {
                taking = true;
                valid = true;
            }
            if( !taking )
            {
                System.out.println("Error. You can only move diagonally when taking a piece.");
            }
        }
        else if(x == super.getX()-1)
        {
            if(piece[x][y] == null)
            {
                if(piece[x-1][y] != null )
                {
                    taking = true;
                    valid = true;
                    piece[x-1][y] = null;
                }
            }
            else
            {
                taking = true;
                valid = true;
            }
            if( !taking )
            {
                System.out.println("Error. You can only move diagonally when taking a piece.");
            }
        }
        else if((y == super.getY()+1 || y == super.getY()-1) && x == super.getX())
        {
            if(piece[x][y] == null)
            {
                System.out.println("Error. You can only take a piece when moving diagonally.");
            }
        }
        else if((y == super.getY()+2 || y == super.getY()-2) && x == super.getX())
        {
            if(piece[x][y] == null)
            {
                System.out.println("Error. You can only take a piece when moving 1 space diagonally forward.");
            }
        }
        else
        {
            valid = true;
        }

        return valid;
    }
    public boolean getMovedTwoLastMove()
    {
        return movedTwoLastMove;
    }
    public String getPiece()
    {
        return "Pawn";
    }
    public String getTeam()
    {
        return super.getTeam();
    }
}

