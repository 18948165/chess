public class Rook extends Piece
{
    private boolean hasMoved;
    public Rook(Piece[][] board, int x, int y, String team, boolean hasMoved)
    {
        super(board, x, y, team, false);
        this.hasMoved = hasMoved;
    }
    public boolean moveTo(int x, int y, Piece[][] board)
    {
        return true;
    }
    public String getPiece()
    {
        return "Rook";
    }
    public String getTeam()
    {
        return super.getTeam();
    }
    public boolean getHasMoved()
    {
        return hasMoved;
    }
}

