public class Piece
{
    private int x;
    private int y;
    private String team;
    private Piece[][] board;
    private boolean taken;
    public Piece(Piece[][] board, int x, int y, String team, boolean taken)
    {
        this.board = board;
        this.x = x;
        this.y = y;
        this.team = team;
        this.taken = taken;
    }
    public void setBoard(Piece[][] board)
    {
        this.board = board;
    }
    public String getTeam()
    {
        return team;
    }
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
}

