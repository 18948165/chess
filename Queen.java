public class Queen extends Piece
{
    public Queen(Piece[][] board, int x, int y, String team)
    {
        super(board, x, y, team, false);
    }
    public boolean moveTo(int x, int y, Piece[][] board)
    {
        return true;
    }
    public String getPiece()
    {
        return "Queen";
    }
    public String getTeam()
    {
        return super.getTeam();
    }
}